# Análisis de Datos sobre la Percepción Social de la Ciencia

Este repositorio contiene un análisis de datos basado en una encuesta realizada a una muestra aleatoria de 550 personas, centrada en la percepción social de la ciencia. La base de datos fue proporcionada por el Semillero de Investigación en Comunicación Científica (SICC). El objetivo del estudio es evaluar cómo perciben las personas la ciencia. El análisis se fundamenta en un cuestionario que incluye preguntas dicotómicas (verdadero/falso) y preguntas en escala de Likert. Estas preguntas permiten evaluar tanto el conocimiento científico básico de los encuestados como sus percepciones y actitudes hacia la ciencia.

Este proyecto busca entender cómo distintos grupos sociales perciben la ciencia para destacar las desigualdades en el acceso a la educación y recursos científicos. Este conocimiento ayuda a promover la diversidad en la investigación científica al incluir variadas perspectivas y experiencias. Además, los análisis realizados sirven para informar a los responsables de políticas públicas, orientando el diseño de proyectos de sensibilización y promoción científica. El objetivo final es fomentar una cultura científica bien estructurada, permitiendo analizar y concienciar sobre el comportamiento de la población frente a la ciencia y desarrollar estrategias para abordar la desinformación y mejorar el entendimiento científico. 

## Estructura del Repositorio

- `Gráficas`: Carpeta que contiene las gráficas que fueron analizadas.
- `Encuesta_SICC_(Responses).xlsx`: Base de datos.
- `Proyecto_Final_FC1.pdf`: Informe tipo artículo que presenta el análisis y las conclusiones derivadas de los datos recopilados. 
- `eda_sicc.ipynb`: El notebook de Jupyter donde se realiza el análisis exploratorio de datos.
- `encuesta_documentation.xlsx`: Documento que contiene un resumen de la base de datos. 
- `requirements.txt`: Archivo que contiene la lista de paquetes y sus versiones que deben ser instalados para que el proyecto funcione correctamente.

## Descripción del Cuestionario

El núcleo principal de la encuesta se compone de las respuestas a 16 preguntas dicotómicas (Verdadero o Falso) para evaluar el conocimiento científico básico de los participantes y 28 preguntas en una escala de Likert. Las primeras 14 de estas preguntas están diseñadas para evaluar la percepción de los encuestados sobre la ciencia, mientras que las otras 14 miden el nivel de información de los encuestados sobre temas específicos. Además, se incluyen los resultados de 14 preguntas de tipo escala de Likert sobre el conocimiento de los encuestados en áreas como modificación genética, nanotecnología, estilo de vida saludable, calentamiento global, agricultura sostenible, vacunas, COVID-19, aproximación científica, divulgación científica, regulaciones a la ciencia, canales de comunicación de los científicos y metodología de la investigación, entre otros. Estas evaluaciones se calificaron en una escala del 1 al 7, donde 1 significa "no estoy informado", 4 "neutro" y 7 "estoy bien informado". 

### Preguntas Dicotómicas
- La tierra gira alrededor del sol. (Verdadero) (dicotomica_vf1)
- La quema de malezas mejora la calidad del suelo para cultivo. (Falso) (dicotomica_vf2)
- Los genes de la madre determinan el género del bebé. (Falso) (dicotomica_vf3)
- Los seres humanos vivieron con dinosaurios. (Falso) (dicotomica_vf4)
- Los antibióticos NO pueden tratar los virus. (Verdadero) (dicotomica_vf5)
- La música clásica puede aumentar la inteligencia de los bebés. (Falso) (dicotomica_vf6)
- Los agroquímicos son productos naturales. (Falso) (dicotomica_vf7)
- A nivel evolutivo, la especie más fuerte es la que sobrevive. (Falso) (dicotomica_vf8)
- La vacunación salva vidas. (Verdadero) (dicotomica_vf9)
- La enfermedad COVID-19 es equivalente a una gripa. (Falso) (dicotomica_vf10)
- Solo se puede investigar en los laboratorios. (Falso) (dicotomica_vf11)
- Lo que dice un científico siempre es verdad. (Falso) (dicotomica_vf12)
- El conocimiento se obtiene únicamente de los libros. (Falso) (dicotomica_vf13)
- La edad es importante a la hora de hacer investigación. (Falso) (dicotomica_vf14)
- Se requiere un conocimiento previo para entender a un científico. (Verdadero) (dicotomica_vf15)
- Toda la investigación es objetiva. (Falso) (dicotomica_vf16)

### Preguntas en Escala de Likert

Preguntas De acuerdo-En desacuerdo tipo escala de Likert:

- Debido a que diferentes científicos usan diferentes conocimientos y métodos, a menudo ofrecen diferentes interpretaciones del mismo fenómeno científico. (agree_scale1)
- Debido a que los científicos son objetivos, siempre ofrecen las mismas interpretaciones del mismo fenómeno. (agree_scale2)
- Una teoría científica a veces se rebate cuando los científicos descubren nuevas evidencias. (agree_scale3)
- Una teoría científica nunca es reemplazada por otra porque se basa en evidencias sólidas. (agree_scale4)
- Los científicos a menudo usan múltiples métodos de investigación para llegar a una conclusión convincente. (agree_scale5)
- Los científicos siempre usan un único método de investigación. (agree_scale6)
- Se puede hacer ciencia por fuera de los laboratorios. (agree_scale7)
- Las personas requieren un pregrado para realizar investigación. (agree_scale8)
- Un científico es una persona que ha estudiado mucho para poder realizar investigación. (agree_scale9)
- Toda la ciencia es buena. (agree_scale10)
- El proceso de investigación es estructurado. (agree_scale11)
- Los científicos solo realizan investigación de una manera. (agree_scale12)
- Todos podemos ser científicos. (agree_scale13)
- Todos los científicos son buenos. (agree_scale14)

Preguntas de Tipo escala de likert (Informado o no informado):
- Modificación genética (informed_scale1)
- Nanotecnología (informed_scale2)
- Estilo de vida saludable (informed_scale3)
- Calentamiento global (informed_scale4)
- Agricultura sostenible (informed_scale5)
- Vacunas (informed_scale6)
- COVID-19 (informed_scale7)
- Cómo sabe un científico qué problemas resolver (informed_scale8)
- Cómo un científico transmite lo que investiga a la comunidad (informed_scale9)
- Regulaciones para las investigaciones científicas (informed_scale10)
- Canales de comunicación de los científicos (informed_scale11)
- Metodología de la investigación (informed_scale12)
- Diferencia entre ciencias exactas, naturales y sociales (informed_scale13)
- Actividades de ciencia en mi entorno (informed_scale14)

## Análisis y Resultados

El análisis incluye la evaluación de la cantidad de respuestas correctas y la percepción de los encuestados en relación con varios factores como la edad, el género, el nivel máximo de educación alcanzado, la clase social, el área de estudio, el tipo de institución educativa (pública o privada), y el país de residencia.

### Principales Conclusiones

A partir del análisis de los resultados se concluyó que, en general, la ciencia se percibe como un campo diverso, dinámico y en constante evolución, en lugar de rígido. Las personas con mayor nivel educativo son más propensas a aceptar que la ciencia no es absoluta y puede tener interpretaciones diversas. Además, estos encuestados muestran un mayor conocimiento sobre temas específicos como el calentamiento global, la nanotecnología y las vacunas. Es curioso que, en promedio, el nivel técnico demuestre un mayor conocimiento en ciertos temas en comparación con el nivel tecnológico, lo que sugiere un posible sesgo en la muestra y la necesidad de ampliar el rango de encuestados.
En cuanto a temas de alto impacto social, como el COVID-19, el conocimiento tiende a ser mayor en niveles educativos más bajos, indicando que la relevancia social influye en la percepción y el conocimiento de la ciencia. Factores como la edad, el nivel educativo y la clase social revelan patrones en el conocimiento científico, aunque no de manera concluyente. Las gráficas sugieren que el nivel tecnológico tiene el mayor promedio de respuestas correctas, lo que puede deberse a sesgos en los datos.

Para un análisis futuro, se recomienda ampliar la muestra, especialmente en categorías de clase social y edad, para obtener una evaluación más representativa y evitar sesgos. Además, factores como la calidad de la educación, el interés personal en la ciencia y la exposición a información científica podrían proporcionar una perspectiva más amplia sobre los elementos que influyen en el conocimiento científico. Finalmente, se sugiere continuar el análisis de la base de datos contrastando las respuestas obtenidas con otros elementos de la encuesta y utilizar la teoría de los campos y el capital social de Pierre Bourdieu para explorar cómo las relaciones sociales influyen en el acceso y la adquisición de conocimientos científicos.

## Cómo Ejecutar el Código

1. Para clonar el repositorio en una máquina local.
   ```bash
   git clone https://gitlab.com/dayana.henaoa/trabajo_final.git

## Autores
Este trabajo se realizó en equipo por las siguientes contribuyentes:

Sara Alejandra Carvajal Ramirez (saraa.carvajal@udea.edu.co)

Valentina Lobo Ruiz (valentina.lobo@udea.edu.co)

Dayana Andrea Henao Arbeláez (dayana.henaoa@udea.edu.co)
